lotatc_inst.options =  {
    use_atc       = true,
    dedicated_mode = true,
    dedicated_profile = "hoggit.lua",
    blue_password   = "hoggit1fw",
    red_password    = "",
    use_chat = true,
    red_max_clients   = 0,
    allowed_users_file = "user_list.json",
    use_admin_commands = true,
    lotatc_link_install = false,
    srs_use_tts         = false,
}
